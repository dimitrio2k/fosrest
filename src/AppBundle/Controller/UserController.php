<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends FOSRestController
{

    public function getUserAction($slug)
    {
        return [$slug];
    }

    /**
     * @Route("/", name="homepage")
     */
    public function getUsersAction()
    {
        $userManager = $this->get('fos_user.user_manager');

        $data = $userManager->findUsers();
        $view = $this->view($data, 200)
            ->setTemplate("MyBundle:Users:getUsers.html.twig")
            ->setTemplateVar('users');

        return $this->handleView($view);
    }

    public function redirectAction()
    {
        $view = $this->redirectView($this->generateUrl('some_route'), 301);
        // or
        $view = $this->routeRedirectView('some_route', array(), 301);

        return $this->handleView($view);
    }
}